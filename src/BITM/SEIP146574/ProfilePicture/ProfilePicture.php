<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $image;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData=Null)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('image',$postVariableData))
        {
            $this->image=$postVariableData['image'];
        }

    }
    public function store()
    {
        $arrData =array($this->name,$this->image);

        $sql="INSERT INTO profile_picture(name,image) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::setMessage("Success!Data has been inserted successfully");
        else
            Message::setMessage("Failed!Data has been inserted successfully");

        Utility::redirect('create.php');
    }
    public function index($fetchMode='ASSOC')
    {
        $sql = "SELECT * from profile_picture where is_deleted = 0 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    // end of index();
    public function view($fetchMode='ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from profile_picture where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public function update()
    {
        if(!empty($this->image)){
            $arrData =array($this->name,$this->image);
            $sql = "UPDATE  profile_picture set name =?,image=? where id=".$this->id;
        }
        else{
            $arrData =array($this->name);
            $sql = "UPDATE  profile_picture set name =? where id=".$this->id;
        }

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }
    public function delete()
    {
        $sql="Delete from profile_picture where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute();
        if($result)
            Message::message("Success!Data has been deleted successfully");
        else
            Message::message("Failed!Data has been deleted successfully");

        Utility::redirect('index.php');
    }
    public function trash()
    {
        $arrData =array($this->name,$this->image);
        $sql = "UPDATE  profile_picture set is_deleted=1 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success!Data has been trushed successfully");
        else
            Message::message("Failed!Data has been trushed successfully");

        Utility::redirect('index.php');
    }
    public function trashList($fetchMode='ASSOC')
    {
        $sql = "SELECT * from profile_picture where is_deleted = 1 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function restore()
    {
        $arrData =array($this->name,$this->image);
        $sql = "UPDATE  profile_picture set is_deleted=0 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success!Data has been restored successfully");
        else
            Message::message("Failed!Data has been restored successfully");

        Utility::redirect('trashList.php');
    }
}